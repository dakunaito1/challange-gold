import { FC, ReactNode, useMemo } from "react";

export interface FeatureCardProps {
  icon: ReactNode;
  title: string;
  description: string;
  iconColor: "yellow" | "red" | "blue" | "green";
}

export const FeatureCard: FC<FeatureCardProps> = ({
  icon,
  iconColor,
  title,
  description,
}) => {
  const bgColor = useMemo(() => {
    switch (iconColor) {
      case "yellow":
        return "bg-yellow-500";

      case "red":
        return "bg-red-500";

      case "blue":
        return "bg-blue-500";

      case "green":
        return "bg-green-500";

      default:
        return "bg-gray-300";
    }
  }, [iconColor]);

  return (
    <div className="p-5 space-y-3 bg-white border border-gray-300 rounded-lg">
      <div
        className={`inline-flex w-8 h-8 text-white items-center justify-center rounded-full text-base ${bgColor}`}
      >
        {icon}
      </div>
      <h3 className="text-base font-semibold">{title}</h3>
      <p className="text-sm text-gray-600 lg:text-base">{description}</p>
    </div>
  );
};
