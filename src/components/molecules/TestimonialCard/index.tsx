import Image from "next/image";
import { FC } from "react";
import StarRatings from "react-star-ratings";

export interface TestimonialCardProps {
  rating: 1 | 2 | 3 | 4 | 5;
  message: string;
  name: string;
  location: string;
  avatar: string;
}

export const TestimonialCard: FC<TestimonialCardProps> = ({
  avatar,
  message,
  name,
  location,
}) => {
  return (
    <div className="flex flex-col items-center gap-1 p-10 space-y-0 rounded-md lg:items-start lg:space-y-3 bg-primary-light lg:flex-row lg:gap-5">
      {avatar && (
        <div className="flex-none">
          <Image
            src={avatar}
            alt="avatar"
            width={80}
            height={80}
            layout="intrinsic"
            objectFit="contain"
          />
        </div>
      )}

      <div className="flex-1 space-y-3">
        <div className="text-center lg:text-left">
          <StarRatings
            rating={4}
            starRatedColor="#F9CC00"
            numberOfStars={5}
            name="rating"
            starDimension="12"
            starSpacing="4"
          />
        </div>

        <p className="text-sm italic text-gray-600 lg:text-base">{`"${message}"`}</p>

        <div className="space-x-1 text-sm font-semibold lg:text-base">
          <span>{name}</span>,<span>{location}</span>
        </div>
      </div>
    </div>
  );
};
