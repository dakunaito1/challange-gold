import { Disclosure } from "@headlessui/react";
import { FC } from "react";
import { FiChevronDown } from "react-icons/fi";

export interface CustomDisclosureProps {
  title: string;
  description: string;
}

export const CustomDisclosure: FC<CustomDisclosureProps> = ({
  title,
  description,
}) => {
  return (
    <Disclosure
      as="div"
      className="w-full px-4 py-3 space-y-3 text-sm border border-gray-300 rounded"
    >
      <Disclosure.Button className="flex items-center justify-between w-full lg:text-base">
        {({ open }) => (
          <>
            <div className="text-left">{title}</div>
            <FiChevronDown
              className={`${open ? "-rotate-90" : "rotate-0"} transition-all`}
            />
          </>
        )}
      </Disclosure.Button>
      <Disclosure.Panel className="text-gray-500">
        {description}
      </Disclosure.Panel>
    </Disclosure>
  );
};
