import { Button } from "@/components/atoms";
import { useAppContext } from "@/contexts/AppContext";
import { Input, Select } from "@mantine/core";
import { useRouter } from "next/router";
import { useCallback } from "react";

export const FormSearch = () => {
  const { push } = useRouter();

  const { setShowSearchModal } = useAppContext();

  const handleClose = useCallback(
    () => setShowSearchModal(false),
    [setShowSearchModal]
  );

  const handleSearch = useCallback(() => {
    push("/products");
    handleClose();
  }, [handleClose, push]);

  return (
    <div className="flex flex-col gap-3 lg:items-end lg:flex-row">
      <Input.Wrapper label="Nama Mobil" className="flex-1">
        <Input placeholder="Ketikan nama mobile" />
      </Input.Wrapper>

      <Select
        className="flex-1"
        label="Kategori"
        placeholder="Masukan kapasitas mobil"
        data={[
          { value: "react", label: "4 - 6 orang" },
          { value: "ng", label: "4 - 8 orang" },
          { value: "svelte", label: "4 - 10 orang" },
        ]}
      />

      <Select
        className="flex-1"
        label="Harga"
        placeholder="Masukan harga sewa per hari"
        data={[
          { value: "react", label: "Rp 100.000" },
          { value: "ng", label: "Rp 200.000" },
          { value: "svelte", label: "Rp 300.000" },
          { value: "vue", label: "Rp 400.000" },
        ]}
      />

      <Select
        className="flex-1"
        label="Status"
        placeholder="Pilih status"
        data={[
          { value: "react", label: "Sewa" },
          { value: "ng", label: "Jual" },
        ]}
      />

      <Button text="Cari mobil" onClick={handleSearch} />
    </div>
  );
};
