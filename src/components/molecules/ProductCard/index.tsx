import { Button } from "@/components/atoms";
import Image from "next/image";
import { useRouter } from "next/router";
import { FC, useCallback } from "react";

export interface ProductCardProps {
  id: string;
  brand: string;
  price: number;
  description: string;
}

export const ProductCard: FC<ProductCardProps> = (props) => {
  const { id, brand, price, description } = props;
  const { push } = useRouter();
  const handleClick = useCallback(() => push(`/products/${id}`), [id, push]);

  return (
    <div className="w-full p-5 bg-white border border-gray-200 rounded-md">
      <Image
        src="/assets/product.png"
        alt="product-image"
        layout="responsive"
        width={900}
        height={598}
        objectFit="cover"
      />

      <div className="space-y-3">
        <h3 className="text-base font-bold capitalize">{brand}</h3>

        <p className="text-base font-semibold text-gray-700">
          <span>Rp {price}</span>
          <span>/ hari</span>
        </p>

        <p className="text-sm text-gray-600 lg:text-base line-clamp-3 lg:line-clamp-4">
          {description}
        </p>

        <Button text="Pilih Mobil" onClick={handleClick} />
      </div>
    </div>
  );
};
