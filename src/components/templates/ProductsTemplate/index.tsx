import {
  DrawerMenu,
  FooterSection,
  HeaderSection,
  ModalSearch,
  ProductsSection,
  SearchSection,
} from "@/components/organisms";

export default function ProductsTemplate() {
  return (
    <>
      <main>
        <HeaderSection />
        <div className="py-5 space-y-5 lg:py-20">
          <SearchSection />
          <ProductsSection />
        </div>
        <FooterSection />
      </main>

      <ModalSearch />
      <DrawerMenu />
    </>
  );
}
