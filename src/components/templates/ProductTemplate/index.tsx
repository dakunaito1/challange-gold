import {
  DrawerMenu,
  FooterSection,
  HeaderSection,
  ModalSearch,
  ProductDetailSection,
  SearchSection,
} from "@/components/organisms";

export default function ProductTemplate() {
  return (
    <>
      <main>
        <HeaderSection />
        <div className="py-5 space-y-5 lg:py-10">
          <SearchSection />
          <ProductDetailSection />
        </div>
        <FooterSection />
      </main>

      <ModalSearch />
      <DrawerMenu />
    </>
  );
}
