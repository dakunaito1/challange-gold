import {
  AboutSection,
  CTASection,
  DrawerMenu,
  FAQSection,
  FeaturesSection,
  FooterSection,
  HeaderSection,
  HeroSection,
  ModalSearch,
  TestimonialSection,
} from "@/components/organisms";

export default function HomeTemplate() {
  return (
    <>
      <main>
        <HeaderSection />
        <HeroSection />

        <div className="py-10 space-y-10 lg:space-y-20 lg:py-20">
          <AboutSection />
          <FeaturesSection />
          <TestimonialSection />
          <CTASection />
          <FAQSection />
          <FooterSection />
        </div>
      </main>

      <DrawerMenu />
      <ModalSearch />
    </>
  );
}
