import {
  FeatureCard,
  FeatureCardProps,
} from "@/components/molecules/FeatureCard";
import { FiAward, FiClock, FiTag, FiThumbsUp } from "react-icons/fi";

const features: FeatureCardProps[] = [
  {
    icon: <FiThumbsUp />,
    iconColor: "yellow",
    title: "Mobil Lengkap",
    description:
      "Tersedia banyak pilihan mobile, kondisi masih baru, bersih dan terawat",
  },
  {
    icon: <FiTag />,
    iconColor: "red",
    title: "Harga Murah",
    description:
      "Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain",
  },
  {
    icon: <FiClock />,
    iconColor: "blue",
    title: "Layanan 24 Jam",
    description:
      "Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu",
  },
  {
    icon: <FiAward />,
    iconColor: "green",
    title: "Sopir Profesional",
    description:
      "Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu",
  },
];

export const FeaturesSection = () => {
  return (
    <section id="why-us" className="container px-5 mx-auto space-y-5 lg:px-0">
      <div className="space-y-0">
        <h2 className="text-lg font-bold capitalize lg:text-xl">Why Us?</h2>

        <p className="text-sm text-gray-600 lg:textbase">
          Mengapa harus pilih Binar Car Rental?
        </p>
      </div>

      <div className="grid grid-cols-1 gap-5 lg:grid-cols-4 lg:gap-10">
        {features.map((item, idx) => (
          <FeatureCard key={idx} {...item} />
        ))}
      </div>
    </section>
  );
};
