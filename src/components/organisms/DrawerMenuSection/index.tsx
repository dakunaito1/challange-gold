/* eslint-disable valid-jsdoc */
import { useAppContext } from "@/contexts/AppContext";
import { pagesData } from "@/data/pagesData";
import { useCallback, useMemo } from "react";
import { FiX } from "react-icons/fi";

/**
 * @deprecated use `DrawerMenu` insted
 * @returns
 */
export const DrawerMenuSection = () => {
  const {
    state: { showDrawerMenu },
    setShowDrawerMenu,
  } = useAppContext();

  const toggle = useMemo(() => {
    if (showDrawerMenu) return "translate-x-0";
    return "translate-x-full";
  }, [showDrawerMenu]);

  const handleClose = useCallback(
    () => setShowDrawerMenu(false),
    [setShowDrawerMenu]
  );

  return (
    <section
      className={`fixed top-0 right-0 z-50 flex items-start justify-start w-full h-screen transition-all ${toggle}`}
    >
      {/* backdrop */}
      <button
        className="flex-1 h-full transition duration-1000 ease-linear delay-700 bg-black cursor-default opacity-80"
        onClick={handleClose}
      />

      <div className="w-1/2 h-full px-5 bg-white">
        <div className="flex items-center justify-between py-5">
          <h2 className="text-lg font-bold uppercase">bcr</h2>
          <button
            className="flex items-center justify-center w-10 h-10 text-lg transition-all rounded-full hover:text-white bg-gray-50 hover:bg-primary"
            onClick={handleClose}
          >
            <FiX />
          </button>
        </div>

        <div className="flex flex-col items-start w-full gap-3">
          {pagesData.map((item) => (
            <button
              className="capitalize transition-all hover:text-primary"
              key={item.name}
            >
              {item.name}
            </button>
          ))}
        </div>
      </div>
    </section>
  );
};
