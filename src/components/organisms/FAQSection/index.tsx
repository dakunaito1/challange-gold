import { CustomDisclosure } from "@/components/molecules";
import { faqData } from "@/data/faqData";

export const FAQSection = () => {
  return (
    <section
      id="faq"
      className="container grid grid-cols-1 gap-5 px-5 mx-auto space-y-5 lg:grid-cols-2 lg:px-0 lg:gap-20"
    >
      <div className="flex-1 space-y-0">
        <h2 className="text-lg font-bold capitalize lg:text-xl">
          Frequently asked question
        </h2>

        <p className="text-sm text-gray-600 lg:text-base">
          Adipisicing nisi sint velit incididunt esse consequat et velit
          proident nisi ipsum Lorem.
        </p>
      </div>

      <div className="flex-1 space-y-3">
        {faqData.map((item) => (
          <CustomDisclosure key={item.title} {...item} />
        ))}
      </div>
    </section>
  );
};
