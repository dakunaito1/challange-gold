import { Button } from "@/components/atoms";

export const CTASection = () => {
  return (
    <section className="container px-5 mx-auto lg:px-0">
      <div className="py-10 space-y-5 text-center text-white rounded-lg lg:py-20 bg-primary">
        <h2 className="text-lg font-bold lg:text-xl">
          Sewa Mobil di Bandung sekarang
        </h2>

        <p className="w-9/12 mx-auto text-sm text-gray-200 lg:w-1/2 lg:text-base">
          Ipsum dolor est minim Lorem exercitation tempor excepteur irure quis
          minim. Duis commodo ipsum incididunt labore do tempor sint mollit
          excepteur eiusmod sint cillum irure voluptate.
        </p>

        <Button text="Mulai Sewa Mobil" />
      </div>
    </section>
  );
};
