import { useAppContext } from "@/contexts/AppContext";
import { pagesData } from "@/data/pagesData";
import { Drawer } from "@mantine/core";
import { useCallback } from "react";

export const DrawerMenu = () => {
  const {
    state: { showDrawerMenu },
    setShowDrawerMenu,
  } = useAppContext();

  const handleClose = useCallback(
    () => setShowDrawerMenu(false),
    [setShowDrawerMenu]
  );

  return (
    <Drawer
      opened={showDrawerMenu}
      title="BCR"
      onClose={handleClose}
      padding="xl"
      size="md"
      position="right"
      styles={{ title: { fontWeight: "bold" } }}
    >
      <section className="flex flex-col items-start w-full gap-3">
        {pagesData.map((item) => (
          <button
            className="capitalize transition-all hover:text-primary"
            key={item.name}
          >
            {item.name}
          </button>
        ))}
      </section>
    </Drawer>
  );
};
