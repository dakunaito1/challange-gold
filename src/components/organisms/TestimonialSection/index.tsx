import { TestimonialCard } from "@/components/molecules";
import { testimonialsData } from "@/data/testimonialsData";
import { Carousel } from "@mantine/carousel";
import { useMantineTheme } from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";

export const TestimonialSection = () => {
  const theme = useMantineTheme();
  const mobile = useMediaQuery(`(max-width: ${theme.breakpoints.sm}px)`);

  const slides = testimonialsData.map((item, idx) => (
    <Carousel.Slide key={idx}>
      <TestimonialCard {...item} />
    </Carousel.Slide>
  ));

  return (
    <section id="testimonial" className="space-y-5">
      <div className="container px-5 mx-auto space-y-0 text-left lg:px-0 lg:text-center">
        <h2 className="text-lg font-bold capitalize lg:text-xl">testimonial</h2>

        <p className="text-sm text-gray-600 lg:text-base">
          Berbagai review positif dari para pelanggan kami
        </p>
      </div>

      <div className="px-5 lg:px-0">
        <Carousel
          slideSize="50%"
          breakpoints={[{ maxWidth: "sm", slideSize: "100%", slideGap: 2 }]}
          slideGap="xl"
          align="center"
          slidesToScroll={mobile ? 1 : 2}
        >
          {slides}
        </Carousel>
      </div>
    </section>
  );
};
