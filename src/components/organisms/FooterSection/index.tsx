import { Logo } from "@/components/atoms";
import { SocialButton } from "@/components/atoms/SocialButton";
import { pagesData } from "@/data/pagesData";
import { ReactNode } from "react";
import {
  FiFacebook,
  FiInstagram,
  FiMail,
  FiTwitch,
  FiTwitter,
} from "react-icons/fi";

const socials: { name: string; icon: ReactNode }[] = [
  { name: "facebook", icon: <FiFacebook /> },
  { name: "instagram", icon: <FiInstagram /> },
  { name: "twitter", icon: <FiTwitter /> },
  { name: "email", icon: <FiMail /> },
  { name: "twitch", icon: <FiTwitch /> },
];

export const FooterSection = () => {
  return (
    <section className="container px-5 mx-auto md:px-0">
      <div className="grid grid-cols-1 gap-5 text-sm md:grid-cols-6">
        <div className="col-span-2">
          <div className="space-y-2">
            <p className="capitalize">
              Jalan Suryo No. 161 Mayangan Kota <br />
              Probolinggo 738163
            </p>
            <p className="font-medium">binarcarrental@gmail.com</p>
            <p className="font-medium">081-1321-313-313</p>
          </div>
        </div>

        <div className="col-span-1">
          <div className="flex flex-col items-start gap-2">
            {pagesData.map((item) => (
              <button key={item.name} className="capitalize hover:text-primary">
                {item.name}
              </button>
            ))}
          </div>
        </div>

        <div className="col-span-2">
          <div className="flex flex-col flex-wrap w-full gap-4">
            <p className="font-medium capitalize">contact with us</p>

            <div className="flex items-center gap-3">
              {socials.map((item) => (
                <SocialButton key={item.name} icon={item.icon} />
              ))}
            </div>
          </div>
        </div>

        <div className="col-span-1">
          <div className="space-y-4">
            <p className="font-semibold">Copyright Binar 2022</p>
            <Logo />
          </div>
        </div>
      </div>
    </section>
  );
};
