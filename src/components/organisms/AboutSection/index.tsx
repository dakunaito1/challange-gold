import { List } from "@/components/atoms";
import Image from "next/image";

const list: string[] = [
  "Sewa mobil dengan supir di Bali 12 jam",
  "Sewa mobil lepas kunci di Bali 24",
  "Sewa mobil jangka panjaang bulanan",
  "Gratis antar - Jemput Mobil di Bandara",
  "Layanan Airport Transfer / Drop in Out",
];

export const AboutSection = () => {
  return (
    <section
      id="our-services"
      className="container flex flex-col gap-5 px-5 mx-auto lg:gap-20 lg:px-0 lg:flex-row lg:items-center"
    >
      <div className="relative w-[459px] h-[428px]">
        <Image
          src="/assets/about.png"
          alt="about-image"
          layout="fill"
          objectFit="contain"
        />
      </div>

      <div className="space-y-3 lg:flex-1">
        <h2 className="text-lg font-bold lg:text-xl">
          Best Car Rental for any kind of trip in Bandung!
        </h2>

        <p className="text-sm text-gray-600 lg:text-base">
          Sewa mobil di Bandung bersama Binar Car Rental jaminan harga lebih
          murah dibandingkan yang lain, kondisi mobil baru, serta kualitas
          pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting,
          dll.
        </p>

        <ul className="space-y-2">
          {list.map((text, idx) => (
            <List key={idx} text={text} />
          ))}
        </ul>
      </div>
    </section>
  );
};
