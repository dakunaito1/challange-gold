import Image from "next/image";
import { FiUsers } from "react-icons/fi";

export const ProductDetailSection = () => {
  return (
    <section className="container grid grid-cols-1 gap-5 px-5 mx-auto lg:grid-flow-col lg:grid-cols-5 lg:px-0">
      <div className="order-2 p-5 space-y-5 border border-gray-300 rounded-md lg:col-span-3 lg:row-span-2 lg:row-start-1 lg:order-1">
        <h2 className="text-lg font-bold capitalize">tentang paket</h2>

        <div className="space-y-3">
          <h3 className="text-base font-semibold capitalize">include</h3>

          <ul className="pl-8 text-sm text-gray-600 list-disc lg:text-base">
            <li>In cupidatat laboris eu tempor aliquip aliquip duis.</li>
            <li>Do anim ex tempor minim.</li>
            <li>SQuis sit do labore cillum labore.</li>
            <li>
              Consequat consectetur occaecat ex cillum aliqua quis anim dolore
              quis irure.
            </li>
          </ul>
        </div>

        <div className="space-y-3">
          <h3 className="text-base font-semibold capitalize">exclude</h3>

          <ul className="pl-8 text-sm text-gray-600 list-disc lg:text-base">
            <li>
              Laboris fugiat elit aliquip quis esse sunt consectetur aliqua
              dolore.
            </li>
            <li>
              Aliquip in eu proident reprehenderit labore elit occaecat sunt.
            </li>
            <li>
              SQui laborum aute aute ipsum non aliquip consequat deserunt esse
              ex ad incididunt Lorem.
            </li>
            <li>
              Aliqua sit in adipisicing occaecat magna exercitation ea
              incididunt enim.
            </li>
          </ul>
        </div>

        <div className="space-y-3">
          <h3 className="text-base font-semibold capitalize">
            refund, reschedule, overtime
          </h3>

          <ul className="pl-8 text-sm text-gray-600 list-disc lg:text-base">
            <li>Ad cillum quis occaecat adipisicing cillum.</li>
            <li>Sint officia reprehenderit id labore sint veniam.</li>
            <li>
              SNon nostrud amet proident ea proident duis in reprehenderit qui
              tempor irure ipsum eu.
            </li>
            <li>Sit cillum adipisicing sint enim culpa.</li>
          </ul>
        </div>
      </div>

      <div className="order-1 col-span-2 p-5 space-y-5 border border-gray-300 rounded-md lg:row-span-1 lg:row-start-1 lg:order-2">
        <Image
          src="/assets/product.png"
          alt="product-detail"
          layout="intrinsic"
          width={900}
          height={598}
          objectFit="contain"
        />

        <h1 className="text-base font-black capitalize lg:text-xl">inova</h1>

        <div className="flex items-center gap-3">
          <FiUsers />
          <span className="text-sm text-gray-600 lg:text-base">
            6 - 8 orang
          </span>
        </div>

        <div className="flex items-center justify-between gap-3 font-semibold capitalize">
          <div>total</div>
          <div>Rp 500.000</div>
        </div>
      </div>
    </section>
  );
};
