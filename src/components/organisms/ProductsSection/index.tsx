import { ProductCard } from "@/components/molecules";
import { productsData } from "@/data/productsData";

export const ProductsSection = () => {
  return (
    <section className="container px-5 mx-auto lg:px-0">
      <div className="grid grid-cols-1 gap-5 lg:grid-cols-3">
        {productsData.map((item) => (
          <ProductCard key={item.id} {...item} />
        ))}
      </div>
    </section>
  );
};
