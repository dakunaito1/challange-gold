import { Button } from "@/components/atoms";
import { useAppContext } from "@/contexts/AppContext";
import Image from "next/image";

export const HeroSection = () => {
  const { setShowSearchModal } = useAppContext();

  return (
    <section
      id="hero"
      className="flex flex-col gap-5 px-5 lg:gap-20 bg-primary-light lg:px-0 lg:flex-row lg:items-center"
    >
      <div className="container flex-1 py-10 mx-auto space-y-5 lg:py-20 lg:relative lg:left-14">
        <h1 className="w-8/12 text-xl font-bold lg:text-3xl">
          Sewa & Rental Mobil Terbaik di Kawasan jakarta
        </h1>

        <p className="text-sm text-gray-600 lg:text-base lg:w-10/12">
          Selamat data di Binar Car Rental. Kami menyediakan mobil kualitas
          terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
          untuk sewa mobil selama 24 jam.
        </p>

        <Button
          text="Mulai Sewa Mobil"
          onClick={() => setShowSearchModal(true)}
        />
      </div>

      <div className="relative lg:flex-1">
        <Image
          src="/assets/mobil.png"
          alt="hero-image"
          layout="responsive"
          width={704}
          height={407}
          objectFit="contain"
          priority
        />
      </div>
    </section>
  );
};
