import { FormSearch } from "@/components/molecules";

export const SearchSection = () => {
  return (
    <section className="container hidden px-5 mx-auto space-y-3 lg:px-0 lg:block">
      <h3 className="text-lg font-bold capitalize lg:text-xl">pencarianmu</h3>

      <div className="p-5 bg-white border border-gray-300 rounded-md">
        <FormSearch />
      </div>
    </section>
  );
};
