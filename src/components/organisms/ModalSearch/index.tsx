import { FormSearch } from "@/components/molecules/FormSearch";
import { useAppContext } from "@/contexts/AppContext";
import { Modal } from "@mantine/core";
import { useCallback } from "react";

export const ModalSearch = () => {
  const {
    state: { showSearchModal },
    setShowSearchModal,
  } = useAppContext();

  const handleClose = useCallback(
    () => setShowSearchModal(false),
    [setShowSearchModal]
  );

  return (
    <Modal
      opened={showSearchModal}
      onClose={handleClose}
      withCloseButton={false}
      size="auto"
      centered
    >
      <FormSearch />
    </Modal>
  );
};
