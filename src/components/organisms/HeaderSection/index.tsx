/* eslint-disable jsx-a11y/anchor-is-valid */
import {
  ActionIconSearch,
  HamburgerMenuButton,
  Logo,
} from "@/components/atoms";
import { useAppContext } from "@/contexts/AppContext";
import Link from "next/link";

import { useCallback } from "react";
import { FiSearch } from "react-icons/fi";

const navigations: { name: string; href: string }[] = [
  { name: "our services", href: "#our-services" },
  { name: "why us", href: "#why-us" },
  { name: "testimonial", href: "#testimonial" },
  { name: "faq", href: "#faq" },
];

export const HeaderSection = () => {
  const { setShowSearchModal } = useAppContext();

  const handleShowModal = useCallback(
    () => setShowSearchModal(true),
    [setShowSearchModal]
  );

  return (
    <section className="sticky inset-x-0 top-0 z-40 py-5 bg-primary-light">
      <div className="container flex items-center justify-between px-5 mx-auto md:px-0">
        <Logo />

        <div className="items-center hidden space-x-5 lg:flex">
          {navigations.map((item) => (
            <Link key={item.name} href={item.href}>
              <a className="text-sm capitalize transition-colors hover:text-primary">
                {item.name}
              </a>
            </Link>
          ))}

          <button
            className="inline-flex items-center gap-2 text-sm capitalize transition-colors hover:text-primary"
            onClick={handleShowModal}
          >
            <FiSearch />
            <span>Search</span>
          </button>
        </div>

        <div className="flex items-center gap-5 lg:hidden">
          <ActionIconSearch />
          <HamburgerMenuButton />
        </div>
      </div>
    </section>
  );
};
