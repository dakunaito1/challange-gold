import { useAppContext } from "@/contexts/AppContext";
import { useCallback } from "react";
import { FiMenu } from "react-icons/fi";

export const HamburgerMenuButton = () => {
  const {
    state: { showDrawerMenu },
    setShowDrawerMenu,
  } = useAppContext();

  const handleClick = useCallback(() => {
    setShowDrawerMenu(!showDrawerMenu);
  }, [setShowDrawerMenu, showDrawerMenu]);

  return (
    <button
      className="flex items-center justify-center text-lg transition-colors bg-white rounded-full md:hidden w-9 h-9 hover:bg-primary hover:text-white"
      onClick={handleClick}
    >
      <FiMenu />
    </button>
  );
};
