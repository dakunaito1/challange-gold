import { FC } from "react";
import { FiCheck } from "react-icons/fi";

interface ListProps {
  text: string;
}

export const List: FC<ListProps> = ({ text }) => {
  return (
    <li className="flex items-center gap-5">
      <div className="flex items-center justify-center w-6 h-6 text-xs rounded-full lg:text-sm text-primary bg-primary-light">
        <FiCheck />
      </div>
      <div className="text-sm lg:text-base">{text}</div>
    </li>
  );
};
