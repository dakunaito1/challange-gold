import { FC, ReactNode } from "react";

export interface SocialButtonProps {
  icon: ReactNode;
}

export const SocialButton: FC<SocialButtonProps> = ({ icon }) => {
  return (
    <button className="flex items-center justify-center w-8 h-8 text-lg text-white transition-all rounded-full hover:bg-primary-light hover:text-primary bg-primary">
      {icon}
    </button>
  );
};
