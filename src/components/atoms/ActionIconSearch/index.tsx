import { useAppContext } from "@/contexts/AppContext";
import { FiSearch } from "react-icons/fi";

export const ActionIconSearch = () => {
  const { setShowSearchModal } = useAppContext();

  return (
    <button
      className="hover:text-primary"
      onClick={() => setShowSearchModal(true)}
    >
      <FiSearch />
    </button>
  );
};
