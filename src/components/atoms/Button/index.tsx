import * as React from "react";

interface Props extends React.HTMLAttributes<HTMLButtonElement> {
  text: string;
}

export const Button: React.FC<Props> = ({ text, ...props }) => {
  return (
    <button
      {...props}
      className="px-3 py-2 text-sm font-semibold text-white transition-colors rounded bg-secondary hover:bg-secondary-light hover:text-secondary"
    >
      {text}
    </button>
  );
};

export default React.memo(Button);
