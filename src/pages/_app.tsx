import { AppContextProvider } from "@/contexts/AppContext";
import type { AppProps } from "next/app";
import * as React from "react";
import "styles/globals.css";
import { MantineProvider } from "@mantine/core";

if (process.env.NEXT_PUBLIC_API_MOCKING === "enabled") {
  import("../mocks").then(({ setupMocks }) => {
    setupMocks();
  });
}

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        /** Put your mantine theme override here */
        colorScheme: "light",
      }}
    >
      <AppContextProvider>
        <Component {...pageProps} />
      </AppContextProvider>
    </MantineProvider>
  );
}
