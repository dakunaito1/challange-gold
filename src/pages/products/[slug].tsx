import ProductTemplate from "@/components/templates/ProductTemplate";

export default function ProductDetailPage() {
  return <ProductTemplate />;
}
