import ProductsTemplate from "@/components/templates/ProductsTemplate";

export default function ProductsPage() {
  return <ProductsTemplate />;
}
