import HomeTemplate from "@/components/templates/HomeTemplate";
import type { NextPage } from "next";

const Home: NextPage = () => {
  return <HomeTemplate />;
};

export default Home;
