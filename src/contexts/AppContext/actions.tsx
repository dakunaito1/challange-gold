import React from "react";
import { DefaultValueType, StateType } from ".";
import { ActionType } from "./reducer";

export const actions = (
  state: StateType,
  dispatch: React.Dispatch<ActionType>
): DefaultValueType => {
  const setShowDrawerMenu = (showDrawerMenu: boolean) => {
    dispatch({ type: "SET_SHOW_DRAWER_MENU", showDrawerMenu });
  };

  const setShowSearchModal = (showSearchModal: boolean) => {
    dispatch({ type: "SET_SHOW_SEARCH_MODAL", showSearchModal });
  };

  return { state, dispatch, setShowDrawerMenu, setShowSearchModal };
};
