import * as React from "react";
import { actions } from "./actions";
import { ActionType, reducer } from "./reducer";

export type StateType = {
  showDrawerMenu: boolean;
  showSearchModal: boolean;
};

const initialState: StateType = {
  showDrawerMenu: false,
  showSearchModal: false,
};

export type DefaultValueType = {
  state: typeof initialState;
  dispatch: React.Dispatch<ActionType>;
  setShowDrawerMenu: (payload: boolean) => void;
  setShowSearchModal: (payload: boolean) => void;
};

const defaultValue: DefaultValueType = {
  state: initialState,
  dispatch: () => undefined,
  setShowDrawerMenu: () => undefined,
  setShowSearchModal: () => undefined,
};

const AppContext = React.createContext(defaultValue);

export const useAppContext = () => React.useContext(AppContext);

interface Props {
  children: React.ReactNode;
}

export const AppContextProvider: React.FC<Props> = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const memoized = React.useMemo(
    () => actions(state, dispatch),
    [state, dispatch]
  );
  return <AppContext.Provider value={memoized}>{children}</AppContext.Provider>;
};
