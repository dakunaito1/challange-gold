import { StateType } from "./";

type SetShowDrawerMenu = {
  type: "SET_SHOW_DRAWER_MENU";
  showDrawerMenu: boolean;
};

type SetShowSearchModal = {
  type: "SET_SHOW_SEARCH_MODAL";
  showSearchModal: boolean;
};

export type ActionType = SetShowDrawerMenu | SetShowSearchModal;

export const reducer = (state: StateType, action: ActionType): StateType => {
  switch (action.type) {
    case "SET_SHOW_DRAWER_MENU":
      return { ...state, showDrawerMenu: action.showDrawerMenu };

    case "SET_SHOW_SEARCH_MODAL":
      return { ...state, showSearchModal: action.showSearchModal };

    default:
      return state;
  }
};
