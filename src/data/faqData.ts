import { CustomDisclosureProps } from "@/components/molecules";

export const faqData: CustomDisclosureProps[] = [
  {
    title: "Apa saja syarat yang dibutuhkan?",
    description:
      "Id amet ea adipisicing ad est. Ut proident ad proident quis commodo aliquip duis labore dolore consectetur. Occaecat incididunt proident sunt incididunt magna dolore deserunt.",
  },
  {
    title: "Berapa hari minimal swa mobil lepas kunci?",
    description:
      "Id amet ea adipisicing ad est. Ut proident ad proident quis commodo aliquip duis labore dolore consectetur. Occaecat incididunt proident sunt incididunt magna dolore deserunt.",
  },
  {
    title: "Berapa hari sebelumnya sebaiknya booking sewa mobil?",
    description:
      "Irure ut proident ut cupidatat labore Lorem ex sint aute elit. Eu nulla irure ex adipisicing mollit et esse. Exercitation aute esse quis exercitation sunt occaecat Lorem commodo aute ex incididunt tempor. Mollit cillum culpa nisi elit voluptate amet nisi anim. Sit amet exercitation excepteur veniam do proident non sunt et fugiat.",
  },
  {
    title: "Apakah ada biaya antar-jemput?",
    description:
      "Veniam Lorem veniam eiusmod esse veniam cupidatat labore tempor quis aliqua do consectetur pariatur do. Amet cillum dolor labore mollit. Tempor deserunt esse ad eiusmod. Nulla veniam mollit irure nisi.",
  },
  {
    title: "Bagaimana jika terjadi kecelakaan?",
    description:
      "Reprehenderit esse tempor qui ad magna commodo mollit amet. Labore ullamco reprehenderit excepteur ipsum officia esse. Labore pariatur pariatur cillum anim tempor sint occaecat aliqua. Non magna magna ipsum minim anim consequat enim mollit cillum elit. Id in et nostrud cillum consectetur officia laborum laborum laboris.",
  },
];
