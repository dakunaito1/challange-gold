import { TestimonialCardProps } from "@/components/molecules";

export const testimonialsData: TestimonialCardProps[] = [
  {
    avatar: "/assets/user-1.png",
    location: "Bandung",
    message:
      "Duis aliqua consequat nisi voluptate ex labore fugiat incididunt qui ut anim. Officia laboris laborum velit enim excepteur. Nisi minim anim adipisicing dolor. Minim officia commodo adipisicing laboris sint consectetur et elit ex.",
    name: "Jhone Doe",
    rating: 5,
  },
  {
    avatar: "/assets/user-2.png",
    location: "Jakarta",
    message:
      "Duis aliqua consequat nisi voluptate ex labore fugiat incididunt qui ut anim. Officia laboris laborum velit enim excepteur. Nisi minim anim adipisicing dolor. Minim officia commodo adipisicing laboris sint consectetur et elit ex.",
    name: "Jane Doe",
    rating: 5,
  },
  {
    avatar: "/assets/user-1.png",
    location: "Surabaya",
    message:
      "Duis aliqua consequat nisi voluptate ex labore fugiat incididunt qui ut anim. Officia laboris laborum velit enim excepteur. Nisi minim anim adipisicing dolor. Minim officia commodo adipisicing laboris sint consectetur et elit ex.",
    name: "James Doe",
    rating: 4,
  },
  {
    avatar: "/assets/user-2.png",
    location: "Bandung",
    message:
      "Duis aliqua consequat nisi voluptate ex labore fugiat incididunt qui ut anim. Officia laboris laborum velit enim excepteur. Nisi minim anim adipisicing dolor. Minim officia commodo adipisicing laboris sint consectetur et elit ex.",
    name: "Jhone Doe",
    rating: 5,
  },
  {
    avatar: "/assets/user-1.png",
    location: "Jakarta",
    message:
      "Duis aliqua consequat nisi voluptate ex labore fugiat incididunt qui ut anim. Officia laboris laborum velit enim excepteur. Nisi minim anim adipisicing dolor. Minim officia commodo adipisicing laboris sint consectetur et elit ex.",
    name: "Jane Doe",
    rating: 5,
  },
  {
    avatar: "/assets/user-2.png",
    location: "Surabaya",
    message:
      "Duis aliqua consequat nisi voluptate ex labore fugiat incididunt qui ut anim. Officia laboris laborum velit enim excepteur. Nisi minim anim adipisicing dolor. Minim officia commodo adipisicing laboris sint consectetur et elit ex.",
    name: "James Doe",
    rating: 4,
  },
];
