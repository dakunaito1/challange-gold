import { ProductCardProps } from "@/components/molecules";

export const productsData: ProductCardProps[] = [
  {
    id: "1",
    brand: "inova",
    price: 500000,
    description:
      "Nostrud commodo excepteur ea adipisicing. Excepteur velit fugiat nisi quis anim sint duis nostrud. Lorem in irure voluptate adipisicing nulla sunt eiusmod magna enim mollit qui dolor do.",
  },
  {
    id: "2",
    brand: "honda",
    price: 600000,
    description:
      "Magna id proident ea Lorem eiusmod fugiat in esse duis anim. Occaecat amet Lorem enim et tempor sit nisi in qui proident. Cupidatat commodo elit esse elit commodo deserunt dolore velit nulla ex dolor velit. Enim fugiat eu pariatur nulla velit pariatur. Sint non voluptate eu excepteur magna adipisicing ipsum deserunt. Anim dolor elit aliquip nostrud laborum aliqua tempor magna adipisicing aliquip eu esse est esse. Ut commodo eu laborum eu nisi velit enim.",
  },
  {
    id: "3",
    brand: "suzuki",
    price: 300000,
    description:
      "Elit cupidatat velit officia excepteur labore quis cupidatat ullamco dolor. Id ullamco in anim deserunt anim anim duis reprehenderit officia aliquip labore. Incididunt consequat minim aute commodo do occaecat in laborum non. Nulla commodo ipsum nostrud mollit consequat tempor velit ex non. Aliqua elit ad ea est fugiat ea irure. Aliquip eiusmod exercitation consectetur tempor consequat labore officia in excepteur voluptate id officia excepteur ea. Laboris in ut officia occaecat.",
  },
];
