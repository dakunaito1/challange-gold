/* eslint-disable @typescript-eslint/no-var-requires */

const defaultTheme = require("tailwindcss/defaultTheme");
const colors = require("tailwindcss/colors");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Inter", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        green: colors.emerald,
        yellow: colors.amber,
        purple: colors.violet,
        gray: colors.neutral,
        primary: {
          light: "#F1F3FF",
          DEFAULT: "#0D28A6",
        },
        secondary: {
          DEFAULT: "#5CB85F",
          light: "#C9E7CA",
        },
      },
    },
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
